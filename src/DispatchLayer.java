import java.util.HashMap;
import java.util.Map;

/**
 * DispatchLayer looks like ServerSocket in the sense that it initiate Server,
 * and accept connections of new clients.
 */

public class DispatchLayer implements Layer { 
	
	// Table linking connectionIds and ConnectionLayers
	// One entry corresponds to one client
	private Map<Integer, ConnectionLayer> clientTable = new HashMap<>(); 
	private int localPort;
	private SemiSynchronizedQueue queue; // Blocking queue to accept clients
	private String supportedVersion;
	private int nbClients;

	// Constructor starts GroundLayer, the server begin to listen
	public DispatchLayer(int localPort, int nbCL) { 
		this.localPort = localPort;
		this.queue = new SemiSynchronizedQueue(nbCL); 
		nbClients = nbCL;
		if (GroundLayer.start(this.localPort)){
			GroundLayer.RELIABILITY = 1;
			GroundLayer.deliverTo(this);
		}
		else {
			// We assume SocketException come from port already in use.
			throw new UnsupportedOperationException("Error in the creation of Groundlayer, try another port");
		}
	}
	
	// Wait for a client to connect and get it's ConnectionLayer to communicate from Server
	public synchronized ConnectionLayer acceptConnection(String version) {
		if (Server.VERBOSE) {System.out.println("Waiting for a client to connect..." +"\n");}
		supportedVersion = version;
	    return (ConnectionLayer) queue.get();
	}

	// Add an entry in the table (new connectionId linked with its connectionLayer)
	public void register(ConnectionLayer layer, int connectionId) {
		clientTable.put(connectionId, layer);
		return;
	}
	
	
	// Parse the connectionId in the payload, check if it exists in the table
	// -> if so, update destination host & port in case of migration and then invoke receive on the layer above
	// -> if not, create new ConnectionLayer for that client and add it to the queue and table
	public void receive(String payload, String source) {
		if (Server.VERBOSE) {System.out.println("Message received by DispatchLayer: " + "Source: "+ source + ", Payload: " + payload + "\n");}
     	
		int clientConnectionId = Integer.parseInt(payload.split(";")[1]); 
		String clientHost = source.split(":")[0].substring(1);
		int clientPort = Integer.parseInt(source.split(":")[1]);
		
		
		// Client already registered
		if(clientTable.containsKey(clientConnectionId)) {
			ConnectionLayer client = clientTable.get(clientConnectionId);
			
			//Connection Migration
			if(client.getDestinationHost() != clientHost){
				client.setDestinationHost(clientHost);
			}
			if(client.getDestinationPort() != clientPort){
				client.setDestinationPort(clientPort);
			}
			
			client.receive(payload, source); 
		} 

		// Client not already registered
		else { 
			// Ckeck how many clients are alredy connected. 
			// If equals to maximum number of clients given by server, silently drop this client.
			if (clientTable.size() < nbClients) {
				// This constructor of ConnectionLayer is specific to the server ("boolean isServer" as last field)
				ConnectionLayer serverConnectionLayer = new ConnectionLayer(this.localPort, clientHost, clientPort, clientConnectionId, supportedVersion, this, true);
				this.register(serverConnectionLayer, clientConnectionId);
				serverConnectionLayer.receive(payload, source); 
				queue.insert(serverConnectionLayer);
			}
		}		
	}

	public void send(String payload) {
		throw new UnsupportedOperationException("Don't use this layer for sending");
	}

	public void deliverTo(Layer above) {
		throw new UnsupportedOperationException("Don't support a single Layer above");
	}

	// Tell server what client(s) are connected
	public Map<Integer, ConnectionLayer> getClientTable() {
		return clientTable;
	}

	public void close() { 
		GroundLayer.close();
	}

	public void removeCL(int connectionId) {
		clientTable.remove(connectionId);		
	}

}


