//import java.io.*;

/**
 *  StreamLayer allow client and server to get data that ConnectionLayer will put in streams.
 *  It works similarly for client and server sides.
 */

public class StreamLayer implements Layer{

	private ConnectionLayer belowLayer = null;
	private int streamId;
	private SemiSynchronizedQueue SLmessageQueue; // List containing StreamLayer messages for endpoints
	/*
	private ByteArrayInputStream in;
	private Reader r;
	private BufferedReader br;
	private ByteArrayOutputStream out;
	private Writer w;
	private BufferedWriter bw;
	private byte[] bufferIn;
	*/

	public StreamLayer(ConnectionLayer below, int Id){
		this.belowLayer = below;
		this.streamId = Id;
		this.SLmessageQueue = new SemiSynchronizedQueue(100);
		/*
		this.bufferIn = new byte[8*1024];

		this.in = new ByteArrayInputStream(bufferIn); //write our own subclass
		this.r = new InputStreamReader(in);
		this.br = new BufferedReader(r, 8*1024);

		this.out = new ByteArrayOutputStream(8*1024); //write our own subclass
		this.w = new OutputStreamWriter(out);
		this.bw = new BufferedWriter(w);
		*/
	}

	// Send packet to below ConnectionLayer with stream header
	public void send(String payload) {
		// TODO: manage streamType
		// Construct type: 1FDOOOSS (see doc P.19)
		String streamType = "10100001";

		// TODO: calculate offset 
		// Add the frame header: type (8) + streamID (8-32) + Offset (0-64) + Data length (0-16) + Data
		payload = streamType + ";" + this.streamId + ";" + "offset;" + payload.length() + ":" + payload;
		if (Server.VERBOSE) {System.out.println("Message sent by StreamLayer: " + payload + "\n");}
		this.belowLayer.send(payload);
	}

	// Add Frame Header for stream 1
	public String handshakeFrameHeader() {
		String streamType = "10100001";
		String payload = "Handshake";
		// Add the frame header: type (8) + streamID (8-32) + Offset (0-64) + Data length (0-16)
		String handshakeFrameHeader = streamType +";" + this.streamId + ";" + "offset;" + payload.length()+ ":" + payload;
		return handshakeFrameHeader;
	}

	// Put message from below ConnectionLayer in the message queue for endpoint
	public void receive(String payload, String source) {
		if (Server.VERBOSE) {System.out.println("Message received by StreamLayer: " + "Source: " + source + ", Payload: " + payload + "\n");}

		// Stream header removal
		payload = payload.substring(payload.indexOf(':')+1);
		SLmessageQueue.insert(payload);
		if (Server.VERBOSE) {System.out.println("Message sent to endpoint: " + "Source: " + source + ", Payload: " + payload + "\n");}
	}

	public void deliverTo(Layer above) {
		throw new UnsupportedOperationException("don't support a single Layer above");
	}

	// Blocking method, return StreamLayer messages when inserted to SLmessageQueue
	public String readMessage(){
		return (String) SLmessageQueue.get();
	}
	
	public int getStreamId(){
		return this.streamId;
	}
	
	public void close() {
		// Do nothing
	}
}
