//import java.io.*;
import java.util.Scanner;

/**
 * Example of Server application layer, using our QUIC API.
 * It has itself another API to communicate with user via terminal (very intuitive).
 * User opens server with "java Client Server myPort nbClient version" where nbClient is the maximum number of connected clients.
 * The server will automatically accept new connections from clients until nbClient is reached.
 * User can create streams, send messages to any user on any stream, and close connection.
 */

public class Server {

	/**
	 * This parameter is very important, we can see "what's happening" inside different layers of QUIC if true.
	 * If false, only application layer messages will appear on the terminal.
	 * We use the same parameter for both client and server.
	 */
	public static final boolean VERBOSE = false;
	
	final static String square = new String(Character.toChars(0x25A0)); // A beautiful character
	
	public static void main(String[] args) {
		if (args.length != 3) {
			System.err.println("syntax : java Server myPort nbClient version");
			return;
		}
		
		if (convertSafe(args[0]) == -1) {
			System.err.println("Pick a valid port number");
			return;
		}
		
		if (convertSafe(args[1]) == -1) {
			System.err.println("Pick a valid maximum number of clients");
			return;
		}
		
		System.out.println("\n******************************* Commands recap ******************************");
		System.out.println("* CREATE_STREAM: create a new stream, you will then have to select a client *");
		System.out.println("* PUBLIC_RESET: send public reset to selected client                        *");
		System.out.println("* CLOSE: close server                                                       *");
		System.out.println("***************************************************************************** \n");
		
		try {
			// Create the DispatchLayer that initiate GroundLayer
			final DispatchLayer server = new DispatchLayer(convertSafe(args[0]), convertSafe(args[1]));
			
			// Thread writing from server to client
			Thread writer = null;
			writer = new Thread(new Runnable() {
				public void run() {
					Scanner sc = new Scanner(System.in);
					ConnectionLayer destinationClient = null;
					int destinationClientId = 0;
					int destinationStreamId;
					System.out.println("Write your message");
					System.out.print("> ");
					while (sc.hasNextLine()) {
						String message = sc.nextLine();
						// Writing CLOSE closes the server
						if (message.equals("CLOSE")) {
							System.out.println("Server closed");
							break;
						} else {
							// Select the client
							System.out.println("Select a client ");
							System.out.println("List of connected clients: " + server.getClientTable().keySet());
							System.out.print("> ");
							boolean clientNotSelected = true;
							while (clientNotSelected) { // Ask user as long as he doesn't give a valid stream
								destinationClientId = convertSafe(sc.nextLine());
								if (server.getClientTable().containsKey(destinationClientId)) {
									destinationClient = server.getClientTable().get(destinationClientId);
									clientNotSelected = false;
								} else {
									System.out.println("Please choose one of these clients: "  + server.getClientTable().keySet());
									System.out.print("> ");
								}
							}
							
							// Writing PUBLIC_RESET does not close the server, but closes client on the other side
							if (message.equals("PUBLIC_RESET")) {
								server.getClientTable().get(destinationClientId).sendPublicReset();
								System.out.println("Public reset sent to client: " + destinationClientId + "\n");
							}else {
								if (message.equals("CREATE_STREAM")) {
									destinationClient.createStream();
								}else {
									// If only stream 0 and 1 are created so far, propose to create one
									if (destinationClient.getStreamTable().keySet().size() == 2) {
										System.out.println("Only stream 0 and 1 are created, and cannot send data.");
										System.out.println("Let's create a third stream for data.");
										System.out.println();
										destinationClient.createStream();
									}
									// Let some time for stream to be created (only to have a beautiful display)
									try {
										Thread.sleep(100);
									} catch (InterruptedException e) {
										e.printStackTrace();
									}
									// Select the stream
									System.out.print("Select a stream ");
									System.out.println("List of streams: " + destinationClient.getStreamTable().keySet());
									System.out.print("> ");

									boolean streamNotSelected = true;
									while (streamNotSelected) { // Ask user as long as he doesn't give a valid stream
										destinationStreamId = convertSafe(sc.nextLine());
										if (destinationClient.getStreamTable().containsKey(destinationStreamId)) {
											if (destinationStreamId == 0 || destinationStreamId == 1) {
												System.out.println("Streams 0 and 1 shouldn't be used for sending messages.");
												System.out.println("Please choose one of these strings: "  + destinationClient.getStreamTable().keySet());
												System.out.print("> ");
											} else { // Send the message
												destinationClient.getStreamTable().get(destinationStreamId).send(message);
												System.out.println("Message sent!\n");
												streamNotSelected = false;
											}
										} else {
											System.out.println("Please choose one of these strings: "  + destinationClient.getStreamTable().keySet());
											System.out.print("> ");
										}
									}
								}
							}
							// Let some time for the ACK to come (only to have a beautiful display)
							try {
								Thread.sleep(100);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							System.out.println("Write your message");
							System.out.print("> ");
						}		
					}
					// Close server
					sc.close();
					server.close();
					System.exit(0);
				}
			});
			writer.start();
			
			// Loop of new client waiting
			while (true) {
				// WARNING: This method blocks the Thread
				// Server only accepts version entered in parameter
				final ConnectionLayer client = server.acceptConnection(args[2]);
				final int clientId = client.getConnectionId();
				System.out.println("\n\nNew client with Id: " + clientId);
								
				// Each client is managed by a thread
				Thread clientManager = new Thread(new Runnable() {
					public void run() {
						String CLmessage;
						// Loop of client message waiting
						while (!Thread.interrupted()) {
							// WARNING: This method blocks the Thread
							CLmessage = client.readMessage();
							System.out.println("\n" + square + "  Message received by server from client " + client.getConnectionId() +": " + CLmessage + "\n");
						
							if (CLmessage.startsWith("Stream created with Id:")) {
								final int streamId = Integer.parseInt(CLmessage.split(":")[1]);
								final StreamLayer stream = client.getStreamLayer(streamId);
								
								// Each stream is managed by a thread
								Thread streamManager = new Thread(new Runnable() {
									public void run() {
										String SLmessage;
										// Loop of stream message waiting
										while (!Thread.interrupted()) {
											// WARNING: This method blocks the Thread
											SLmessage = stream.readMessage();
											System.out.println("\n" + square + "  Message received by server from client " + client.getConnectionId() 
													+ " from stream with Id " + stream.getStreamId() + ": " + SLmessage + "\n");
										}
									}
								});
								streamManager.start();
							}
						}
					}	
				});
				clientManager.start();
			}
		} catch (UnsupportedOperationException e) {
			// Exception thrown from DispatchLayer constructor if GroundLayer.start fails
			System.err.println(e);
			return;
		}
	}
	
	private static int convertSafe(String s) {
		try {
			return Integer.parseInt(s);
		} catch (NumberFormatException e) { // nothing
		    return -1;
		}
	}
}

