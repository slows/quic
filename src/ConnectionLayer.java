import java.util.HashMap;
import java.util.Map;

/**
 * ConnectionLayer looks like Socket in the sense that it is used by both Client and Server to communicate.
 * It behaves slightly differently (upon construction, version negotiation and closing) depending on the layer below (Ground for Client or Dispatch for Server).
 */

public class ConnectionLayer implements Layer { 

	private int localPort;
	private String destinationHost;
	private int destinationPort;
	private int connectionId;
	private String supportedVersion;
	private DispatchLayer dispatchLayer; // Allow to remove ConnectionLayer from DispatchLayer client table when closing
	private boolean isServer; // Indicate if client-side or server-side
	
	private StreamLayer stream0; // Reserved for connection-level flow control 
	private StreamLayer stream1; // Reserved for handshake & version negotiation

	private int streamId; // Odd for clients and even for server
	private int packetNbr; // Count packets 

	// Table linking streamIds and StreamLayers
	// One entry corresponds to one stream
	private Map<Integer, StreamLayer> streamTable = new HashMap<>(); 

	private boolean CCSent = false; // Avoid ConnectionClose loops

	private SemiSynchronizedQueue CLmessageQueue; // List containing ConnectionLayer messages for endpoints


	// Client-side constructor, no "boolean isServer" as last field
	// It starts GroundLayer, creates basic streams and sends handshake
	public ConnectionLayer(int localPort, String destHost, int destPort, int Id, String version) { 
		this.localPort = localPort;
		this.destinationHost = destHost;
		this.destinationPort = destPort;
		this.connectionId = Id;
		this.isServer = false;
		
		this.streamId = 3; // Client have odd-numbered StreamIDs
		this.packetNbr = 1; // Initialize packetNbr
		this.CLmessageQueue = new SemiSynchronizedQueue(100); // List containing messages

		if (GroundLayer.start(this.localPort)) {
			GroundLayer.RELIABILITY = 1;
			GroundLayer.deliverTo(this);

			// Creation of the streams with Id 0 for connection-level flow control 
			stream0 = this.createStreamById(0);
			// Creation of the streams with Id 1 for handshake & version negotiation
			stream1 = this.createStreamById(1);

			this.sendHandshake(version);
		}
		else {
			// We assume SocketException come from port already in use.
			throw new UnsupportedOperationException("Error in the creation of Groundlayer, try another port");
		}
	}

	// Server-side constructor, "boolean isServer" as last field
	// Idem than Client-side constructor but doesn't start GroundLayer and doesn't send handshake.
	public ConnectionLayer(int localPort, String destHost, int destPort, int Id, String supVersion, DispatchLayer dispatch, boolean isServer) {
		this.localPort = localPort;
		this.destinationHost = destHost;
		this.destinationPort = destPort;
		this.connectionId = Id;
		this.supportedVersion = supVersion;
		this.dispatchLayer = dispatch;
		this.isServer = true;
			
		this.streamId = 2; // Server have even-numbered StreamIDs
		this.packetNbr = 1; // Initialize packetNbr
		this.CLmessageQueue = new SemiSynchronizedQueue(100); // List containing messages

		// Creation of the streams with Id 0 for connection-level flow control 
		stream0 = this.createStreamById(0);
		// Creation of the streams with Id 1 for handshake & version negotiation
		stream1 = this.createStreamById(1);
	}


	// Send packet to GroundLayer
	// For lite version, only 1 frame per packet
	public void send(String payload) { 
		// Adds the Regular Packet header = flags(8) + connectionId(64) + packet number + Data
		payload = "NoFlag;" + connectionId + ";" + packetNbr + ":" + payload;
		this.packetNbr++;
		if (Server.VERBOSE) {System.out.println("Message sent by ConnectionLayer: " + payload + "\n");}
		GroundLayer.send(payload, this.destinationHost, this.destinationPort);

		// If too many packets, close connection
		if (packetNbr > (java.lang.Math.pow(2,64)-1) && !CCSent) {
			CCSent = true;
			sendConnectionClose("QUIC_SEQUENCE_NUMBER_LIMIT_REACHED", "You went too far bro...");
		}
	}
	
	// Client tries to initiate a connection
	public void sendHandshake(String version) { 
		// Adds the Frame Header from stream 1
		String payload = stream1.handshakeFrameHeader();
		// Adds the Regular Packet header with version = flags(8) + connectionId(64) + version(32) + packet number + Data
		payload = "VersionFlag;" + connectionId + ";" + version + ";" + packetNbr + ":" + payload;
		this.packetNbr++;
		if (Server.VERBOSE) {System.out.println("Message sent by ConnectionLayer: " + payload + "\n");}
		GroundLayer.send(payload, this.destinationHost, this.destinationPort);
	}

	// Version Negociation Packet
	// Tells the client what version the server supports
	public void sendVersionNegotiation() { 
		String payload = "VersionFlag;" + connectionId + ";" + supportedVersion;
		if (Server.VERBOSE) {System.out.println("Message sent by ConnectionLayer: " + payload + "\n");}
		GroundLayer.send(payload, this.destinationHost, this.destinationPort);
	}

	// Public Reset Packet
	public void sendPublicReset() { 
		String payload = "PublicResetFlag;" + connectionId + ";" + "CryptoCrap";
		if (Server.VERBOSE) {System.out.println("Message sent by ConnectionLayer: " + payload + "\n");}
		GroundLayer.send(payload, this.destinationHost, this.destinationPort);
		this.close();
	}
	
	// Regular Packet with ACK Frame
	private void sendACK(int largestAcked) {
		// No Ack Block section and no Timestamp section for lite version
		String payload = "01000101;" + largestAcked + ";" + "ThisIsAnAck";
		send(payload);
	}
	
	// Regular Packet with Connection Close frame
	public void sendConnectionClose(String errorCode, String reasonPhrase) {
		String payload = "00000010;" + errorCode + ";" + reasonPhrase.length() + ";" + reasonPhrase;
		CCSent = true; // Avoid connection close loops
		send(payload);
		CLmessageQueue.insert("Error: " + errorCode + ", reason for closing: " + reasonPhrase);
		this.close();
	}

	// This is the most complicated method of our QUIC, depending on the flag and frame type many things can happen
	public void receive(String payload, String source) {
		if (Server.VERBOSE) {System.out.println("Message received by ConnectionLayer: " + "Source: " + source + ", Payload: " + payload + "\n");}

		// Get flag
		String flag = payload.split(";")[0];

		// Cf P.9 for the flag state machine
		if (flag.equals("PublicResetFlag")) { 
			// Close ConnectionLayer when receive PUBLIC_RESET
			this.close();
			return;
		}
		
		if (flag.equals("VersionFlag")) {
			if (isServer) { // Server-side
				if (payload.split(";")[2].equals(supportedVersion)) {
					// Version is supported, answer with VersionFlag off
					stream1.send("VersionSupported");
				} else {
					// Answer Version Negotiation packet with VersionFlag and set of supportedVersions
					this.sendVersionNegotiation();
				}
			} else { // Client-side
				// Version Negotiation Packet from server, client must close to enter a valid version
				sendConnectionClose("QUIC_INVALID_VERSION", "Unsupported version");
			}
			return;
		}

		// Now we know this is a Regular Packet, with no QUIC Version in Header
		// Get Packet Number
		int receivedPacketNbr = Integer.parseInt(payload.split(":")[0].split(";")[2]);
		
		// Packet header removal
		payload = payload.substring(payload.indexOf(':')+1);

		// get Frame Type
		String frameType = payload.split(";")[0];

		if (frameType.startsWith("1")) { // This is a stream
			// Identification of stream
			int receivedStreamId = Integer.parseInt(payload.split(";")[1]);
			
			// Stream already registered, bring up the payload
			if(streamTable.containsKey(receivedStreamId)) {
				streamTable.get(receivedStreamId).receive(payload, source); 
			} 
			// Stream not already registered, create new stream and bring up the payload
			else { 
				StreamLayer newStreamLayer = this.createStreamById(receivedStreamId);
				newStreamLayer.receive(payload, source);
			}
			// Send an ACK 
			sendACK(receivedPacketNbr);
			return;
		}

		if (frameType.startsWith("01")) { // This is an ACK
			int lastAcked = Integer.parseInt(payload.split(";")[1]);
			if (Server.VERBOSE) {System.out.println("Acked packetNbr " + lastAcked + " with received packetNbr " + receivedPacketNbr + "\n");}
			return;
		}
		
		if (frameType.equals("00000010")) { // This is a CONNECTION_CLOSE
			if (Server.VERBOSE) {System.out.println("Error: " + payload.split(";")[1] + " Reason for closing: " + payload.split(";")[3] + "\n");}
			
			// Insert message for endpoint in queue
			CLmessageQueue.insert("Error: " + payload.split(";")[1] + ", reason for closing: " + payload.split(";")[3]);
			sendACK(receivedPacketNbr);
			this.close();
			return;
		}
	}


	public void deliverTo(Layer aboveLayer) {
		throw new UnsupportedOperationException(
				"don't support a single Layer above");
	}


	// Create a new stream with auto-labeled streamId
	public StreamLayer createStream() {
		StreamLayer streamLayer = new StreamLayer(this, streamId); 
		streamTable.put(streamId, streamLayer);
		if (Server.VERBOSE) {System.out.println("Stream created with Id " + streamId + " for client with Id " + connectionId + "\n");}
		
		// Insert message for endpoint in queue
		CLmessageQueue.insert("Stream created with Id:" + streamId);
		this.streamId+=2;
		return streamLayer;
	}


	// Create a new stream with given streamId
	public StreamLayer createStreamById(int givenStreamId) {
		StreamLayer streamLayer = new StreamLayer(this, givenStreamId); 
		streamTable.put(givenStreamId, streamLayer);
		if (Server.VERBOSE) {System.out.println("Stream created with Id " + givenStreamId + " for client with Id " + connectionId + "\n");}
		
		// Insert message for endpoint in queue
		CLmessageQueue.insert("Stream created with Id:" + givenStreamId);
		return streamLayer;
	}

	// Blocking method, return ConnectionLayer messages when inserted to CLmessageQueue
	public String readMessage(){
		return (String) CLmessageQueue.get();
	}
	
	// Tell endpoint what streams are opened
	public Map<Integer, StreamLayer> getStreamTable() {
		return streamTable;
	}
	
	public StreamLayer getStreamLayer(int streamId){ 
		if (streamTable.containsKey(streamId)) {
			return (StreamLayer) streamTable.get(streamId);
		}
		else {
			throw new UnsupportedOperationException(
					"no stream with streamId " + streamId);
		}
	}

	public int getLocalPort() {
		return localPort;
	}

	public void setLocalPort(int localPort) {
		this.localPort = localPort;
	}

	public String getDestinationHost() {
		return destinationHost;
	}

	public void setDestinationHost(String destinationHost) {
		this.destinationHost = destinationHost;
	}

	public int getDestinationPort() {
		return destinationPort;
	}

	public void setDestinationPort(int destinationPort) {
		this.destinationPort = destinationPort;
	}

	public int getConnectionId() {
		return connectionId;
	}

	public void close() {
		// Insert message for endpoint in queue
		CLmessageQueue.insert("CONNECTION CLOSED");
		if (isServer){
			dispatchLayer.removeCL(connectionId);
		} else {
			// Close GroundLayer only for client, server must keep it open for new clients.
			GroundLayer.close();
		}
	}
}
