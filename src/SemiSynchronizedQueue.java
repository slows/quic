class SemiSynchronizedQueue{

	private Object[] elements;
	private int capacity;						// Maximum number of elements
	
	private int inQueue;						// How many elements are there actually?
	private int readPos, writePos;				// Indexes of from where to read, and write.
	private boolean debug;
	
	public SemiSynchronizedQueue(int queueSize){
		elements = new Object[queueSize];
		capacity = queueSize;
		inQueue  = 0;
		readPos  = 0;
		writePos = 0;
		debug    = false;
	}
	
	public SemiSynchronizedQueue(int queueSize, boolean d){
		elements = new Object[queueSize];
		capacity = queueSize;
		inQueue  = 0;
		readPos  = 0;
		writePos = 0;
		debug    = d;
	}
	
	public synchronized void insert(Object object){
		if (inQueue == capacity){	
			return; //If the queue is full, new entries are just dropped
		}
		// At this point, there should be at least one slot in the queue. That slot
		// is at "writePos".

		if (debug){
			System.out.print("++ Inserting element in position "+writePos +". Old size: " +inQueue+ ". "); 
		}
		
		elements[writePos] = object;		// Add a reference to the received object to the queue.
		inQueue  = inQueue  +1 ; 	// And now there's one more element than before
		
		writePos = writePos +1 ; 	// point to where to write the next element
								 	// oh, but wait, what if that is "beyond the 
		if (writePos >= capacity){	// table boundaries"? We need to check, and reset if so
			writePos = 0;
		}
		if (debug){
			System.out.println("New size: " +inQueue+ ". Next element added at position "+writePos); 
		}

		// What if there's already an element at the new writePos?
		// Well, that doesn't change the fact that the index given would 
		// be where to write if the queue had room (i.e., if inQueue < capacity).
		
		// Anyways, we've added an element to the queue. Let's notify anybody waiting
		notify();
	}
	
	public synchronized Object get(){
		while ( inQueue == 0){		// Anything in the queue?
			try{ wait(); } catch (InterruptedException e){}; // no, then suspend thread and wait
		}
		// When we get to here, we know that there is an element to read,
		// and we know where to find it: at readPos.
		//
		// So, we need to get that element, and then we need to increment readPos
		// to point to the next element - taking caring about wraparound etc.
		
		int elementToReturn = readPos; 	// The element that we will return
		inQueue = inQueue-1;			// We've removed (well, we are removing) one element.
		readPos = readPos +1 ; 	    	// Then, point to from where to read the next element
								 		// oh, but wait, what if that is "beyond the 
		if (readPos >= capacity){		// table boundaries"? We need to check,
			readPos = 0;				// and reset if so
		}		
		// What if there's no element at the new readPos?
		// Well, that doesn't change the fact that the index given would 
		// be from where to read if there weere elements in the queue (i.e., if inQueue >0).
		// In this particular case, btw., readPos == writePos.
		//

		if (debug){
			System.out.println("-- Getting element from position "+elementToReturn +". Old size:" +(inQueue+1) +". New size: " +inQueue+ ". Next element read from position "+readPos); 
		}

		return(elements[elementToReturn]);
	}
}
