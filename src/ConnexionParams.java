public class ConnexionParams {
	
	private String remoteHost; //to be modified
	private int remotePort;
	private int serverId;
	private String payload;
	private String source;
	private int remoteId;
	
	public ConnexionParams(String remoteHost, int remotePort, int serverId, String payload, String source, int remoteId){
		this.remoteHost = remoteHost;
		this.remotePort = remotePort;
		this.serverId = serverId;
		this.payload = payload;
		this.source = source;
		this.remoteId = remoteId;
	}

	public String getRemoteHost() {
		return remoteHost;
	}

	public int getRemotePort() {
		return remotePort;
	}

	public int getServerId() {
		return serverId;
	}

	public String getPayload() {
		return payload;
	}

	public String getSource() {
		return source;
	}
	
	public int getRemoteId() {
		return remoteId;
	}

}
