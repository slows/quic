import java.util.Scanner;

/**
 * Example of Client application layer, using our QUIC API.
 * It has itself another API to communicate with user via terminal (very intuitive).
 * User can connect to a server with "java Client myPort destinationHost destinationPort version"
 * Then he can create streams, send messages on any stream and close connection.
 */

public class Client {
	
	final static String square = new String(Character.toChars(0x25A0)); // A beautiful character

	public static void main(String[] args) {

		if (args.length != 4) {
			System.err
			.println("syntax : java Client myPort destinationHost destinationPort version");
			return;
		}
		
		if (convertSafe(args[0]) == -1 || convertSafe(args[2]) == -1) {
			System.err.println("Pick a valid port number");
			return;
		}

		try {
			// Client opens a connection, this creates two streams dedicated to:
			// Stream with Id 0: connection-level flow control
			// Stream with Id 1: handshake & version negotiation

			// This constructor of ConnectionLayer is specific to the client (no "boolean isServer" as last field)
			// ConnectionId is picked randomly as integer lower than Integer.MAX_VALUE.
			// Here we use only 100 to make it user-friendly.
			final ConnectionLayer myTalk = new ConnectionLayer(convertSafe(args[0]), args[1],
					convertSafe(args[2]), (int) (Math.random() * 100), args[3]);
			
			System.out.println("\n************************ Commands recap **********************");
			System.out.println("* CREATE_STREAM: create a new stream                         *");
			System.out.println("* PUBLIC_RESET: send public reset to server and close client *");
			System.out.println("************************************************************** \n");
			
			// Thread writing from client to server
			Thread writer = null;
			writer = new Thread(new Runnable() {
				public void run() {
					Scanner sc = new Scanner(System.in);
					int destinationStreamId;
					System.out.println("Write your message");
					System.out.print("> ");
					while (sc.hasNextLine()) {
						String message = sc.nextLine();
						// Writing PUBLIC_RESET closes the client, but doesn't crash the server 
						if (message.equals("PUBLIC_RESET")) {
							myTalk.sendPublicReset();
							break;
						}else {
							if (message.equals("CREATE_STREAM")) {
								myTalk.createStream();
							}else {
								// If only stream 0 and 1 are created so far, propose to create one
								if (myTalk.getStreamTable().keySet().size() == 2) {
									System.out.println("Only stream 0 and 1 are created, and cannot send data.");
									System.out.println("Let's create a third stream for data.");
									myTalk.createStream();
								} 
								// Let some time for stream to be created (only to have a beautiful display)
								try {
									Thread.sleep(100);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
								// Select the stream
								System.out.println("Select a stream.");
								System.out.println("List of streams: " + myTalk.getStreamTable().keySet());
								System.out.print("> ");

								boolean streamNotSelected = true;
								while (streamNotSelected) { // Ask user as long as he doesn't give a valid stream
									destinationStreamId = convertSafe(sc.nextLine());
									if (myTalk.getStreamTable().containsKey(destinationStreamId)) {
										if (destinationStreamId == 0 || destinationStreamId == 1) {
											System.out.println("Streams 0 and 1 shouldn't be used for sending messages.");
											System.out.println("Please choose one of these strings: "  + myTalk.getStreamTable().keySet());
											System.out.print("> ");
										} else { // Send the message
											myTalk.getStreamTable().get(destinationStreamId).send(message);
											System.out.println("Message sent!\n");
											streamNotSelected = false;
										}
									} else {
										System.out.println("Please choose one of these strings: "  + myTalk.getStreamTable().keySet());
										System.out.print("> ");
									}
								}	
							}
							// Let some time for the ACK to come (only to have a beautiful display)
							try {
								Thread.sleep(100);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							System.out.println("Write your message");
							System.out.print("> ");
						}		
					}
					// Close server
					sc.close();
					System.exit(0);
				}
			});
			writer.start();
			
				
			// Loop of server message waiting
			String CLmessage;
			while (!Thread.interrupted()) {
				// WARNING: This method blocks the Thread
				CLmessage = myTalk.readMessage();
				System.out.println("\n" + square + "  Message received from server by client "	+ myTalk.getConnectionId() + ": " + CLmessage + "\n");

				if (CLmessage.equals("CONNECTION CLOSED")) {
					// Close client if receive a CONNECTION CLOSED
					System.exit(0);
				}
				if (CLmessage.startsWith("Stream created with Id:")) {
					final int streamId = Integer.parseInt(CLmessage.split(":")[1]);
					final StreamLayer stream = myTalk.getStreamLayer(streamId);

					// Each stream is managed by a thread
					Thread streamManager = new Thread(new Runnable() {
						public void run() {
							String SLmessage;
							// Loop of stream message waiting
							while (!Thread.interrupted()) {
								// WARNING: This method blocks the Thread
								SLmessage = stream.readMessage();
								System.out.println("\n" + square + "  Message received by server from client " + myTalk.getConnectionId() 
										+ " from stream with Id " + stream.getStreamId() + ": " + SLmessage + "\n");
							}
						}
					});
					streamManager.start();
				}
			}		
		} catch (UnsupportedOperationException e) {
			// Exception thrown from ConnectionLayer constructor if GroundLayer.start fails
			System.err.println(e);
			return;
		}
	}
	
	private static int convertSafe(String s) {
		try {
			return Integer.parseInt(s);
		} catch (NumberFormatException e) { // nothing
		    return -1;
		}
	}
}


